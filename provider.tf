provider "google" {
  project = var.project_id
  credentials = var.google_cloud_credential #https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#adding-credentials
  region  = "us-central1"
}

provider "google-beta" {
  credentials = var.google_cloud_credential
  project = var.project_id
  region  = "us-central1"
}
