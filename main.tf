// VPC Network
resource "google_compute_network" "vpc_network" {
  name = "sahmed-scratchpad-cluster-network-01"
  auto_create_subnetworks = true
}

//GKE Cluster
resource "google_container_cluster" "sahmed_scratchpad_gke_cluster" {
  name     = "sahmed-scratchpad-cluster"
  location = "us-central1-c"
  network = google_compute_network.vpc_network.name

  private_cluster_config {
    enable_private_nodes = true
    master_ipv4_cidr_block="10.162.128.32/28"
    enable_private_endpoint = false
  }

  ip_allocation_policy {
    cluster_ipv4_cidr_block = "10.64.0.0/20"
    services_ipv4_cidr_block = "10.32.0.0/20"
  }

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
}

//GKE nodes
// discount: https://cloud.google.com/compute/docs/instances/preemptible
resource "google_container_node_pool" "sahmed_scratchpad_gke_n1_nodes" {
  name       = "sahmed-scratchpad-cluster-n1-pool"
  location   = "us-central1-c"
  cluster    = google_container_cluster.sahmed_scratchpad_gke_cluster.name
  autoscaling {
    min_node_count = 1
    max_node_count = 2
  }
  node_config {
    preemptible  = true
    machine_type = "n1-standard-1"
    disk_size_gb = 30
    disk_type = "pd-standard"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.sahmed_scratchpad_gke_nodes_service_account.email
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}
// free tier: https://cloud.google.com/compute/docs
resource "google_container_node_pool" "sahmed_scratchpad_gke_e2_nodes" {
  name       = "sahmed-scratchpad-cluster-e2-pool"
  cluster    = google_container_cluster.sahmed_scratchpad_gke_cluster.name
  location   = "us-central1-c"
  node_count = 1
  node_config {
    preemptible  = false
    machine_type = "e2-micro"
    disk_size_gb = 30
    disk_type = "pd-standard"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.sahmed_scratchpad_gke_nodes_service_account.email
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}

// https://cloud.google.com/nat/docs/gke-example#create-nat
resource "google_compute_router" "router" {
  project = var.project_id
  name    = "nat-router"
  network = google_compute_network.vpc_network.name
  region  = "us-central1"
}

module "cloud-nat" {
  source                             = "terraform-google-modules/cloud-nat/google"
  version                            = "~> 2.0.0"
  project_id                         = var.project_id
  region                             = "us-central1"
  router                             = google_compute_router.router.name
  name                               = "nat-config"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}
