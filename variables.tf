variable "google_cloud_credential" {
  description = "the contents of a service account key file in JSON format."
  type = string
}
#gcp
variable "project_id" {
  description = "project_id"
  type = string
  default = "dev-package-container-96a3ff34"
}
