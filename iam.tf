# create service account for gke cluster node pool
resource "google_service_account" "sahmed_scratchpad_gke_nodes_service_account" {
  project = var.project_id
  account_id   = "sahmed-scratchpad-gke-nodes"
  display_name = "sahmed-scratchpad-gke-nodes-service-account"
}

# give service account for gke cluster node pool logwritting and metricwritting roles
resource "google_project_iam_member" "sahmed_scratchpad_gke_nodes_service_account-logwriter" {
  project = var.project_id
  role   = "roles/logging.logWriter"
  member = "serviceAccount:${google_service_account.sahmed_scratchpad_gke_nodes_service_account.email}"
}

resource "google_project_iam_member" "sahmed_scratchpad_gke_nodes_service_account-metricwriter" {
  project = var.project_id
  role    = "roles/monitoring.metricWriter"
  member  = "serviceAccount:${google_service_account.sahmed_scratchpad_gke_nodes_service_account.email}"
}

# create service account for helmsman deeploymnts
resource "google_service_account" "sahmed_scratchpad_gke_helmsman_service_account" {
  project      = var.project_id
  account_id   = "helmsman-deployer"
  display_name = "sahmed-scratchpad-gke-helmsman-deployer-sa"
}

# give helmsman deeploymnt service account Kubernetes Engine admin to deploy workloads to clusters
resource "google_project_iam_member" "sahmed_scratchpad_gke_helmsman_role-k8s-engine-developer" {
  project = var.project_id
  role    = "roles/container.admin"
  member  = "serviceAccount:${google_service_account.sahmed_scratchpad_gke_helmsman_service_account.email}"
}
